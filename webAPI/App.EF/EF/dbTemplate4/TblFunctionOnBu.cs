﻿using System;
using System.Collections.Generic;

namespace App.EF.EF.dbTemplate4
{
    public partial class TblFunctionOnBu
    {
        public int CBuid { get; set; }
        public int CFunctionId { get; set; }
        public int? CStatus { get; set; }
    }
}
